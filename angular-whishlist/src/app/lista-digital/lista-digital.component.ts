import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { DestinoDigital } from './../models/destino-digital.model';
import { DestinosApiClient } from './../models/destinos-api-client.model';


@Component({
  selector: 'app-lista-digital',
  templateUrl: './lista-digital.component.html',
  styleUrls: ['./lista-digital.component.css']
})
export class ListaDigitalComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoDigital>;
  updates: string[];
  constructor(public destinosApiClient:DestinosApiClient) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.destinosApiClient.subscribeOnChange((d: DestinoDigital) => {
      if(d != null){
        this.updates.push('se ha elegido a ' + d.nombre);
      }
    })
  }

  ngOnInit(): void {
  }
  agregado(d: DestinoDigital) {
  	
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
    return false;
  }

  elegido(e: DestinoDigital) {
   // this.destinosApiClient.getAll().forEach(x => x.setSelected(false));
    this.destinosApiClient.elegir(e);
  }
}
