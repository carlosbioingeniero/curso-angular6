import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaDigitalComponent } from './lista-digital.component';

describe('ListaDigitalComponent', () => {
  let component: ListaDigitalComponent;
  let fixture: ComponentFixture<ListaDigitalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaDigitalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaDigitalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
