import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DestinoDigitalComponent } from './destino-digital.component';

describe('DestinoDigitalComponent', () => {
  let component: DestinoDigitalComponent;
  let fixture: ComponentFixture<DestinoDigitalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DestinoDigitalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DestinoDigitalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
