import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoDigital } from '../models/destino-digital.model';


@Component({
  selector: 'app-destino-digital',
  templateUrl: './destino-digital.component.html',
  styleUrls: ['./destino-digital.component.css']
})
export class DestinoDigitalComponent implements OnInit {
  @Input() digital: DestinoDigital;
  @Input('idx') position: number;

  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() clicked: EventEmitter<DestinoDigital>;
  
  constructor() {
    this.clicked = new EventEmitter();
  }

  ngOnInit():void {
  }

  ir(){
    this.clicked.emit(this.digital);
    return false;
  }
}
