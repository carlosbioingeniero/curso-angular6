import {DestinoDigital } from './destino-digital.model';
import { Subject, BehaviorSubject} from 'rxjs';

export class DestinosApiClient {
  digitales: DestinoDigital[];
  current: Subject<DestinoDigital> = new BehaviorSubject<DestinoDigital>(null);
  constructor() {
    this.digitales = [];
  }
  add(d:DestinoDigital){
    this.digitales.push(d);
  }
  getAll(): DestinoDigital[] {
    return this.digitales;
  }
  getById(id:String): DestinoDigital{
	  return this.digitales.filter(function(d) {return d.id.toString() === id;})[0];
    }
    elegir(d: DestinoDigital){
      this.digitales.forEach(x => x.setSelected(false));
      d.setSelected(true);
      this.current.next(d);
  }

  subscribeOnChange(fn){
      this.current.subscribe(fn);
  }
  }