import {v4 as uuid} from 'uuid';
export class DestinoDigital{
     private selected: boolean;
     public  servicios: string[];
     id = uuid();
        constructor(public nombre: string, public u: string){
            this.servicios=['Valoracion gratuita','Informe detallado'];
        }
        isSelected(): boolean{
            return this.selected;
        }
        setSelected(s: boolean) {
            this.selected= s;
        }
}
