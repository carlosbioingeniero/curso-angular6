import { Component, OnInit } from '@angular/core';
import { DestinosApiClient } from './../models/destinos-api-client.model';
import { DestinoDigital } from './../models/destino-digital.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css']
})
export class DestinoDetalleComponent implements OnInit {
  digital:DestinoDigital;

  constructor(private route: ActivatedRoute, private destinosApiClient:DestinosApiClient) {}

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.digital = this.destinosApiClient.getById(id);
  }

}
