import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormDestinoDigitalComponent } from './form-destino-digital.component';

describe('FormDestinoDigitalComponent', () => {
  let component: FormDestinoDigitalComponent;
  let fixture: ComponentFixture<FormDestinoDigitalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormDestinoDigitalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormDestinoDigitalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
