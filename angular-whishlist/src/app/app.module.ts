import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';



import { AppComponent } from './app.component';
import { DestinoDigitalComponent } from './destino-digital/destino-digital.component';
import { ListaDigitalComponent } from './lista-digital/lista-digital.component';
import { DestinoDetalleComponent } from './destino-detalle/destino-detalle.component';
import { FormDestinoDigitalComponent } from './form-destino-digital/form-destino-digital.component';
import { DestinosApiClient } from './models/destinos-api-client.model';

const routes: Routes = [
  { path: '', redirectTo:'home', pathMatch: 'full'},
  { path: 'home', component: ListaDigitalComponent},
  { path: 'destino', component: DestinoDetalleComponent},

];
@NgModule({
  declarations: [
    AppComponent,
    DestinoDigitalComponent,
    ListaDigitalComponent,
    DestinoDetalleComponent,
    FormDestinoDigitalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [DestinosApiClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
